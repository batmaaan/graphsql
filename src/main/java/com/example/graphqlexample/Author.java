package com.example.graphqlexample;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
class Author {
  @Id
  @GeneratedValue
  private Long id;
  private String name;
  @OneToMany(mappedBy = "author", cascade = CascadeType.ALL)
  private List<Book> books = new ArrayList<>();


  public Author(Long id, String name) {
    this.id = id;
    this.name = name;
  }
  public Author() {
  }

  public Long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public List<Book> getBooks() {
    return books;
  }
}
