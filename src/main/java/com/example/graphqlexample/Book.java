package com.example.graphqlexample;


import javax.persistence.*;

@Entity
class Book {
  @Id
  @GeneratedValue
  private Long id;
  private String title;
  private String publisher;
  @ManyToOne(fetch = FetchType.LAZY)
  private Author author;

  public Book() {
  }

  public Book(String title, String publisher, Author author) {
    this.title = title;
    this.publisher = publisher;
    this.author = author;
  }

  public Long getId() {
    return id;
  }

  public String getTitle() {
    return title;
  }

  public String getPublisher() {
    return publisher;
  }

}
