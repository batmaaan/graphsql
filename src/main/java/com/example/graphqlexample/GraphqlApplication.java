package com.example.graphqlexample;

import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;

@SpringBootApplication
public class GraphqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(GraphqlApplication.class, args);
	}

  @Bean
  ApplicationRunner applicationRunner(AuthorRepository authorRepository, BookRepository bookRepository) {
    return args -> {
      Author josh = authorRepository.save(new Author(null, "josh"));
      Author mark = authorRepository.save(new Author(null, "mark"));
      bookRepository.saveAll(List.of(
        new Book("title1", "publisher1", josh),
        new Book("title2", "publisher2", josh),
        new Book("title3", "publisher3", mark)

      ));
    };
  }
}
